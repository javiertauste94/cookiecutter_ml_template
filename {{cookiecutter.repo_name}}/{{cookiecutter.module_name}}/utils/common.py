{{cookiecutter.signature}}


""" common.py
"""


import os
import pandas as pd
from src.config import {{cookiecutter.params_file}}
from typing import Union, Tuple


def get_arguments(parameters: list) -> Tuple(str, str):
    """
    Try to read the data path and the column the known parameters
    Args:
        parameters: sys.argv. List of parameters passed.

    Returns:

    """
    try:
        # sys.argv[1]  # Program name
        param1 = parameters[1]
        param2 = parameters[2]
    except IndexError:
        param1 = "data/train.csv"
        param2 = "column_name"  # string, int or None
    return param1, param2
