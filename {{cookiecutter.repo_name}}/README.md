# {{cookiecutter.project_name}}
{{cookiecutter.description}}

### Just in case:
Terminologies:
- **Train data**: % of the entire CSV. Evaluation data is included here. 
Transformations are learned from this data. 
- **Test data**: Rest of the CSV. Used for simulate and evaluate a real 
prediction. Transformations learned from train are only applied. 

Methodology: <br>

>![Methodology ML flow](docs/figures/Methodology_MLflow.jpg "Methodology ML flow")

## Structure

	{{cookiecutter.repo_name}}
	│
	├── logs
	│
	├── data
	│   │
	│   ├── processed                                   <- Processed and clean data of /raw.
	│   │
	│   └── raw                                         <- Initial CSV stored data.
	│
	├── docs
	│   │
	│   ├── figures
	│	│
	│   ├── sphinx                                      <- sphinx-doc.org or other documentation generator.
	│   │
	│   ├── references                                  <- Manuals, links and other explanatory materials.
	│   │
	│   └── reports                                     <- Generated analysis as HTML, PDF, LaTeX, etc.
	│      
	│						                <- Source/module folder. There can be multiple if needed.
	├── {{cookiecutter.module_name}}
	│   │
	│   ├── config                                      <- Configurtion files.
	│   │   │
	│   │   └── {{cookiecutter.params_file}}.py
	│   │
	│   ├── notebooks                                   <- Jupyter notebooks. Naming convention is a number (for ordering),
	│   │   │                                              the creator's initials, and a short `-` delimited description,
	│   │   │                                              e.g: `1.0-jqp-initial-data-exploration`.
	│   │   │
	│   │   ├── data_exploration                        <- Inspection and descriptive analysis.
	│   │   │
	│   │   └── experiments                             <- Little functionalities experimentation.
	│   │ 
	│   ├── test                                        <- Process, predict and evaluate test data.
	│   │   │
	│   │   ├── __init__.py
	│   │   │
	│   │   ├── test_model.py
	│   │   │
	│   │   └── test_process.py
	│   │
	│   ├── train                                       <- Process, configure a model and train on test data.
	│   │   │
	│   │   ├── {{cookiecutter.data_transforms}}        <- Saved data transformations. Same naming convention as notebooks.
	│   │   │
	│   │   ├── models                                  <- .py files with model architectures.
	│   │   │   │
	│   │   │   ├── {{cookiecutter.trained_models}}     <- zoo of saved trained models. Same naming convention as notebooks.
	│   │   │   │
	│   │   │   ├── __init__.py
	│   │   │   │
	│   │   │   └── get_models.py                       <- multiple model architectures to choose.
	│   │   │
	│   │   ├── __init__.py
	│   │   │
	│   │   ├── train_model.py
	│   │   │
	│   │   └── train_process.py
	│   │
	│   ├── utils                                       <- Auxiliary functions.
	│   │   │
	│   │   ├── common.py
	│   │   │
	│   │   └── __init__.py
	│   │
	│   └── __init__.py
	│
	├── {{cookiecutter.main_file}}.py                   <- Main script to execute. It manages all module executions
	│
	├── .gitignore
	│
	├── README.md                                       <- This file.
	│
	└── requirements.txt                                <- The requirements file for reproducing the analysis environment,
                                                               e.g. generated with `pip freeze > requirements.txt`


## Configuration
Parameters configured by analysts are located in the config folder.
Parameters configured by users are feed to the program via console
arguments or hard-coded in main.py. These are path to train data, 
columns to predict, etc.

## Cookiecutter
To create a new tree hierarchy you must type in a terminal:
> cookiecutter 'PATH_TO_JSON_COOKIECUTTER' 

Autor: Solver Intelligent Analytics
