{{cookiecutter.signature}}


""" {{cookiecutter.main_file}}.py
"""


from src.config import {{cookiecutter.params_file}}


def {{cookiecutter.main_file}}():
    return


if __name__ == '__main__':
    {{cookiecutter.main_file}}()
